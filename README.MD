## Информация о проекте
Task Manager

## Сведения о разработчике
Мавлютов Филипп | fmavlyutov@t1-consulting.ru

## Сборка программы
```
> mvn clean install
```
## Запуск программы
```
> java -jar ./java-course-task-18.jar
```

## Требования

#### Hardware:
1. CPU Intel Core i7-7700K
2. RAM 16GB

#### Software:
1. Windows 10 x64
2. JDK Version 1.8

## Поддерживаемые команды
- [**help** | **-h**] - display help
- [**version** | **-v**] - display version
- [**about** | **-a**] - display author
- [**info** | **-i**] - display system info
- [**commands** | **-cmd**] - display all commands
- [**arguments** | **-arg**] - display all arguments
- [**project-create**] - create new project
- [**project-list**] - display list of projects
- [**project-clear**] - delete all project
- [**project-show-by-id**] - show project by id
- [**project-show-by-index**] - show project by index
- [**project-update-by-id**] - update project by id
- [**project-update-by-index**] - update project by index
- [**project-remove-by-id**] - delete project by id
- [**project-remove-by-index**] - delete project by index
- [**project-start-by-id**] - start project by id
- [**project-start-by-index**] - start project by index
- [**project-complete-by-id"**] - complete project by id
- [**project-complete-by-index**] - complete project by index
- [**project-change-status-by-id**] - change project status by id
- [**project-change-status-by-index**] - change project status by index
- [**task-create**] - create new task
- [**task-list**] - display list of tasks
- [**task-clear**] - delete all tasks
- [**task-show-by-id**] - show task by id
- [**task-show-by-index**] - show task by index
- [**task-update-by-id**] - update task by id
- [**task-update-by-index**] - update task by index
- [**task-remove-by-id**] - delete task by id
- [**task-remove-by-index**] - delete task by index
- [**task-start-by-id**] - start task by id
- [**task-start-by-index**] - start task by index
- [**task-complete-by-id"**] - complete task by id
- [**task-complete-by-index**] - complete task by index
- [**task-change-status-by-id**] - change task status by id
- [**task-change-status-by-index**] - change project task by index
- [**task-bind-to-project**] - bind task to project
- [**task-unbind-from-project**] - unbind task from project
- [**task-show-by-project-id**] - show task by project id
- [**login**] - login user
- [**logout**] - logout user
- [**user-registry**] - registry user
- [**user-change-password**] - change user password
- [**user-update-profile**] - update user profile
- [**user-view-profile**] - view user profile
- [**exit**] - exit program
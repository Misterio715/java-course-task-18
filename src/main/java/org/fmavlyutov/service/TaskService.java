package org.fmavlyutov.service;

import org.fmavlyutov.api.repository.ITaskRepository;
import org.fmavlyutov.api.service.ITaskService;
import org.fmavlyutov.enumerated.Sort;
import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.exception.entity.EmptyTaskException;
import org.fmavlyutov.exception.entity.TaskNotFoundException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIdException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIndexException;
import org.fmavlyutov.exception.field.InvalidOrEmptyNameException;
import org.fmavlyutov.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task add(final Task task) {
        if (task == null) {
            throw new EmptyTaskException();
        }
        return taskRepository.add(task);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(Comparator comparator) {
        if (comparator == null) {
            return findAll();
        }
        return taskRepository.findAll(comparator);
    }

    @Override
    public List<Task> findAll(Sort sort) {
        if (sort == null) {
            return findAll();
        }
        return findAll(sort.getComparator());
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) {
            throw new InvalidOrEmptyNameException();
        }
        final Task task = new Task();
        task.setName(name);
        if (description != null) {
            task.setDescription(description);
        }
        return add(task);
    }

    @Override
    public Task findOneById(final String id) {
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        return taskRepository.findOneById(id);
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (index == null || index < 0 || index >= taskRepository.getSize()) {
            throw new InvalidOrEmptyIndexException();
        }
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        if (name == null | name.isEmpty()) {
            throw new InvalidOrEmptyNameException();
        }
        final Task task = findOneById(id);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0 || index >= taskRepository.getSize()) {
            throw new InvalidOrEmptyIndexException();
        }
        if (name == null | name.isEmpty()) {
            throw new InvalidOrEmptyNameException();
        }
        final Task task = findOneByIndex(index);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public void remove(final Task task) {
        if (task == null) {
            throw new EmptyTaskException();
        }
        taskRepository.remove(task);
    }

    @Override
    public Task removeById(final String id) {
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(final Integer index) {
        if (index == null || index < 0 || index >= taskRepository.getSize()) {
            throw new InvalidOrEmptyIndexException();
        }
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task changeStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        final Task task = taskRepository.findOneById(id);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0 || index >= taskRepository.getSize()) {
            throw new InvalidOrEmptyIndexException();
        }
        final Task task = taskRepository.findOneByIndex(index);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        task.setStatus(status);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(String id) {
        if (id == null || id.isEmpty()) {
            return Collections.emptyList();
        }
        return taskRepository.findAllByProjectId(id);
    }

}

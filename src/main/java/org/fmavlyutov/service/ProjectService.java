package org.fmavlyutov.service;

import org.fmavlyutov.api.repository.IProjectRepository;
import org.fmavlyutov.api.service.IProjectService;
import org.fmavlyutov.enumerated.Sort;
import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.exception.entity.EmptyProjectException;
import org.fmavlyutov.exception.entity.ProjectNotFoundException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIdException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIndexException;
import org.fmavlyutov.exception.field.InvalidOrEmptyNameException;
import org.fmavlyutov.model.Project;

import java.util.Comparator;
import java.util.List;

public final class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project add(final Project project) {
        if (project == null) {
            throw new EmptyProjectException();
        }
        return projectRepository.add(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Comparator comparator) {
        if (comparator == null) {
            return findAll();
        }
        return projectRepository.findAll(comparator);
    }

    @Override
    public List<Project> findAll(final Sort sort) {
        if (sort == null) {
            return findAll();
        }
        return findAll(sort.getComparator());
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) {
            throw new InvalidOrEmptyNameException();
        }
        final Project project = new Project();
        project.setName(name);
        if (description != null) {
            project.setDescription(description);
        }
        return add(project);
    }

    @Override
    public Project findOneById(final String id) {
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        return projectRepository.findOneById(id);
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        if (index == null || index < 0 || index >= projectRepository.getSize()) {
            throw new InvalidOrEmptyIndexException();
        }
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        if (name == null | name.isEmpty()) {
            throw new InvalidOrEmptyNameException();
        }
        final Project project = findOneById(id);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0 || index >= projectRepository.getSize()) {
            throw new InvalidOrEmptyIndexException();
        }
        if (name == null | name.isEmpty()) {
            throw new InvalidOrEmptyNameException();
        }
        final Project project = findOneByIndex(index);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public void remove(final Project project) {
        if (project == null) {
            throw new EmptyProjectException();
        }
        projectRepository.remove(project);
    }

    @Override
    public Project removeById(final String id) {
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        return projectRepository.removeById(id);
    }

    @Override
    public Project removeByIndex(final Integer index) {
        if (index == null || index < 0 || index >= projectRepository.getSize()) {
            throw new InvalidOrEmptyIndexException();
        }
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project changeStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        final Project project = projectRepository.findOneById(id);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0 || index >= projectRepository.getSize()) {
            throw new InvalidOrEmptyIndexException();
        }
        final Project project = projectRepository.findOneByIndex(index);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        project.setStatus(status);
        return project;
    }

}

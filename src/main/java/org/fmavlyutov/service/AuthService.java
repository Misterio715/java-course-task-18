package org.fmavlyutov.service;

import org.fmavlyutov.api.service.IAuthService;
import org.fmavlyutov.api.service.IUserService;
import org.fmavlyutov.exception.field.InvalidOrEmptyLoginException;
import org.fmavlyutov.exception.field.InvalidOrEmptyPasswordException;
import org.fmavlyutov.exception.user.AccessDeniedException;
import org.fmavlyutov.model.User;
import org.fmavlyutov.util.HashUtil;

public final class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public User registry(String login, String password, String email) {
        return userService.create(login, password, email);
    }

    @Override
    public void login(String login, String password) {
        if (login == null || login.isEmpty()) {
            throw new InvalidOrEmptyLoginException();
        }
        if (password == null | password.isEmpty()) {
            throw new InvalidOrEmptyPasswordException();
        }
        final User user = userService.findByLogin(login);
        if (user == null) {
            throw new AccessDeniedException();
        }
        final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) {
            throw new AccessDeniedException();
        }
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public String getUserId() {
        if (!isAuth()) {
            throw new AccessDeniedException();
        }
        return userId;
    }

    @Override
    public User getUser() {
        if (!isAuth()) {
            throw new AccessDeniedException();
        }
        final User user = userService.findById(userId);
        if (user == null) {
            throw new AccessDeniedException();
        }
        return user;
    }

}

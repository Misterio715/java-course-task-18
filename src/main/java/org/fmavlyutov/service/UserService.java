package org.fmavlyutov.service;

import org.fmavlyutov.api.repository.IUserRepository;
import org.fmavlyutov.api.service.IUserService;
import org.fmavlyutov.enumerated.Role;
import org.fmavlyutov.exception.entity.UserNotFoundException;
import org.fmavlyutov.exception.field.InvalidOrEmptyEmailException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIdException;
import org.fmavlyutov.exception.field.InvalidOrEmptyLoginException;
import org.fmavlyutov.exception.field.InvalidOrEmptyPasswordException;
import org.fmavlyutov.exception.user.EmptyRoleException;
import org.fmavlyutov.exception.user.ExistsEmailException;
import org.fmavlyutov.exception.user.ExistsLoginException;
import org.fmavlyutov.model.User;
import org.fmavlyutov.util.HashUtil;

import java.util.List;

public final class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) {
            throw new InvalidOrEmptyLoginException();
        }
        if (isLoginExists(login)) {
            throw new ExistsLoginException();
        }
        if (password == null || password.isEmpty()) {
            throw new InvalidOrEmptyPasswordException();
        }
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return userRepository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) {
            throw new InvalidOrEmptyLoginException();
        }
        if (isLoginExists(login)) {
            throw new ExistsLoginException();
        }
        if (password == null || password.isEmpty()) {
            throw new InvalidOrEmptyPasswordException();
        }
        if (isEmailExists(email)) {
            throw new ExistsEmailException();
        }
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) {
            throw new InvalidOrEmptyLoginException();
        }
        if (isLoginExists(login)) {
            throw new ExistsLoginException();
        }
        if (role == null) {
            throw new EmptyRoleException();
        }
        final User user = create(login, password);
        if (role != null) {
            user.setRole(role);
        }
        return user;
    }

    @Override
    public User add(final User user) {
        if (user == null) {
            throw new UserNotFoundException();
        }
        return userRepository.add(user);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(final String id) {
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        final User user = userRepository.findById(id);
        if (user == null) {
            return null;
        }
        return user;
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) {
            throw new InvalidOrEmptyLoginException();
        }
        final User user = userRepository.findByLogin(login);
        if (user == null) {
            return null;
        }
        return user;
    }

    @Override
    public User findByEmail(final String email) {
        if (email == null || email.isEmpty()) {
            throw new InvalidOrEmptyEmailException();
        }
        final User user = userRepository.findByEmail(email);
        if (user == null) {
            return null;
        }
        return user;
    }

    @Override
    public User remove(final User user) {
        if (user == null) {
            throw new UserNotFoundException();
        }
        return userRepository.remove(user);
    }

    @Override
    public User removeById(final String id) {
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        final User user = userRepository.findById(id);
        return remove(user);
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) {
            throw new InvalidOrEmptyLoginException();
        }
        final User user = userRepository.findByLogin(login);
        return remove(user);
    }

    @Override
    public User removeByEmail(final String email) {
        if (email == null || email.isEmpty()) {
            throw new InvalidOrEmptyEmailException();
        }
        final User user = userRepository.findByEmail(email);
        return remove(user);
    }

    @Override
    public User setPassword(final String id, final String password) {
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        if (password == null || password.isEmpty()) {
            throw new InvalidOrEmptyPasswordException();
        }
        final User user = findById(id);
        if (user == null) {
            throw new UserNotFoundException();
        }
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User updateUser(final String id, final String firstName, final String lastName, final String middleName) {
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        final User user = findById(id);
        if (user == null) {
            throw new UserNotFoundException();
        }
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public Boolean isLoginExists(final String login) {
        if (login == null || login.isEmpty()) {
            return false;
        }
        return userRepository.isLoginExists(login);
    }

    @Override
    public Boolean isEmailExists(final String email) {
        if (email == null || email.isEmpty()) {
            return false;
        }
        return userRepository.isEmailExists(email);
    }

}

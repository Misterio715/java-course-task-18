package org.fmavlyutov.api.model;

import org.fmavlyutov.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}

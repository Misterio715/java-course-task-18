package org.fmavlyutov.api.repository;

import org.fmavlyutov.model.User;

import java.util.List;

public interface IUserRepository {

    User add(User user);

    List<User> findAll();

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User remove(User user);

    Boolean isLoginExists(String login);

    Boolean isEmailExists(String email);

}

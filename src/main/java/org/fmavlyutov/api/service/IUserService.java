package org.fmavlyutov.api.service;

import org.fmavlyutov.api.repository.IUserRepository;
import org.fmavlyutov.enumerated.Role;
import org.fmavlyutov.model.User;

public interface IUserService extends IUserRepository {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User removeById(String id);

    User removeByLogin(String login);

    User removeByEmail(String email);

    User setPassword(String id, String password);

    User updateUser(String id, String firstName, String lastName, String middleName);

}

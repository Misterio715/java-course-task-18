package org.fmavlyutov.util;

import org.fmavlyutov.exception.system.ParseNumberException;

import java.util.Scanner;

import static java.lang.System.in;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextInt() {
        try {
            return Integer.parseInt(SCANNER.nextLine());
        } catch (NumberFormatException e) {
            throw new ParseNumberException(e);
        }
    }

}

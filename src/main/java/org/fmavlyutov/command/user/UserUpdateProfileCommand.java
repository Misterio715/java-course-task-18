package org.fmavlyutov.command.user;

import org.fmavlyutov.model.User;
import org.fmavlyutov.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "update user profile";
    }

    @Override
    public String getName() {
        return "user-update-profile";
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[UPDATE USER PROFILE]");
        System.out.println("Enter first name:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter last name:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter middle name:");
        final String middleName = TerminalUtil.nextLine();
        final User user = getUserService().updateUser(userId, firstName, lastName, middleName);
        showUser(user);
    }

}

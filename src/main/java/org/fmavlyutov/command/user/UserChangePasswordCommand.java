package org.fmavlyutov.command.user;

import org.fmavlyutov.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "change user password";
    }

    @Override
    public String getName() {
        return "user-change-password";
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[CHANGE USER PASSWORD]");
        System.out.println("Enter new password");
        final String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }

}

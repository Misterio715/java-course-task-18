package org.fmavlyutov.command.user;

import org.fmavlyutov.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "view user profile";
    }

    @Override
    public String getName() {
        return "user-view-profile";
    }

    @Override
    public void execute() {
        final User user = getAuthService().getUser();
        System.out.println("[VIEW USER PROFILE]");
        showUser(user);
    }

}

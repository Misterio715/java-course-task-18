package org.fmavlyutov.command.user;

import org.fmavlyutov.api.service.IAuthService;
import org.fmavlyutov.api.service.IUserService;
import org.fmavlyutov.command.AbstractCommand;
import org.fmavlyutov.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected IAuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

    protected IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    protected void showUser(final User user) {
        if (user == null) {
            return;
        }
        System.out.println(user);
    }

}

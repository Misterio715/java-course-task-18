package org.fmavlyutov.command.user;

public final class UserLogoutCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "logout user";
    }

    @Override
    public String getName() {
        return "logout";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT USER]");
        getAuthService().logout();
    }

}

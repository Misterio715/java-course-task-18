package org.fmavlyutov.command.system;

import org.fmavlyutov.command.AbstractCommand;

public final class SystemCommandListCommand extends AbstractSystemCommand {

    @Override
    public String getArgument() {
        return "-cmd";
    }

    @Override
    public String getDescription() {
        return "display all commands";
    }

    @Override
    public String getName() {
        return "commands";
    }

    @Override
    public void execute() {
        for (final AbstractCommand command : getCommandService().getTerminalCommands()) {
            if (command == null) {
                continue;
            }
            String name = command.getName();
            if (name == null || name.isEmpty()) {
                continue;
            }
            System.out.println(name);
        }
        System.out.println();
    }

}

package org.fmavlyutov.command.system;

import org.fmavlyutov.api.service.ICommandService;
import org.fmavlyutov.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

}

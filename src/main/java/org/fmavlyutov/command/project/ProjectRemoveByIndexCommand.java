package org.fmavlyutov.command.project;

import org.fmavlyutov.model.Project;
import org.fmavlyutov.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "delete project by index";
    }

    @Override
    public String getName() {
        return "project-remove-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextInt() - 1;
        final Project project = getProjectService().removeByIndex(index);
        getProjectTaskService().removeProjectById(project.getId());
    }

}

package org.fmavlyutov.command.project;

import org.fmavlyutov.enumerated.Sort;
import org.fmavlyutov.model.Project;
import org.fmavlyutov.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "show list of projects";
    }

    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECTS LIST]");
        System.out.println("Enter sort:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        final List<Project> projects = getProjectService().findAll(Sort.toSort(sort));
        int index = 1;
        for (final Project project : projects) {
            if (project == null) {
                continue;
            }
            System.out.println(index++ + ". " + project.getName());
        }
    }

}

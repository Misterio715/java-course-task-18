package org.fmavlyutov.command.project;

import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.model.Project;
import org.fmavlyutov.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "change project status by index";
    }

    @Override
    public String getName() {
        return "project-change-status-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("Enter index:");
        Integer index = TerminalUtil.nextInt() - 1;
        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String status = TerminalUtil.nextLine();
        final Project project = getProjectService().changeStatusByIndex(index, Status.toStatus(status));
        showProject(project);
    }

}

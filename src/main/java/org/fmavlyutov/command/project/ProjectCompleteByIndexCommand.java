package org.fmavlyutov.command.project;

import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.model.Project;
import org.fmavlyutov.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "complete project by index";
    }

    @Override
    public String getName() {
        return "project-complete-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextInt() - 1;
        final Project project = getProjectService().changeStatusByIndex(index, Status.COMPLETED);
        showProject(project);
    }

}

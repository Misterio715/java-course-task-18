package org.fmavlyutov.command.project;

import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.model.Project;
import org.fmavlyutov.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "complete project by id";
    }

    @Override
    public String getName() {
        return "project-complete-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().changeStatusById(id, Status.COMPLETED);
        showProject(project);
    }

}

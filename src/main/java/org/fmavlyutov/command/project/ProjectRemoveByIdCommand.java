package org.fmavlyutov.command.project;

import org.fmavlyutov.model.Project;
import org.fmavlyutov.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "delete project by id";
    }

    @Override
    public String getName() {
        return "project-remove-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().removeById(id);
        getProjectTaskService().removeProjectById(project.getId());
    }

}

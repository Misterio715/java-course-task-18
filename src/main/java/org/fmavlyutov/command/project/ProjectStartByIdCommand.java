package org.fmavlyutov.command.project;

import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.model.Project;
import org.fmavlyutov.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "start project by id";
    }

    @Override
    public String getName() {
        return "project-start-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().changeStatusById(id, Status.IN_PROGRESS);
        showProject(project);
    }

}

package org.fmavlyutov.command.task;

import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.model.Task;
import org.fmavlyutov.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "change task status by index";
    }

    @Override
    public String getName() {
        return "task-change-status-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("Enter index:");
        String id = TerminalUtil.nextLine();
        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String status = TerminalUtil.nextLine();
        final Task task = getTaskService().changeStatusById(id, Status.toStatus(status));
        showTask(task);
    }

}

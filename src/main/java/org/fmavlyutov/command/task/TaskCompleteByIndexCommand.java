package org.fmavlyutov.command.task;

import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.model.Task;
import org.fmavlyutov.util.TerminalUtil;

public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "complete task by index";
    }

    @Override
    public String getName() {
        return "task-complete-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextInt() - 1;
        final Task task = getTaskService().changeStatusByIndex(index, Status.COMPLETED);
        showTask(task);
    }

}

package org.fmavlyutov.command.task;

import org.fmavlyutov.enumerated.Sort;
import org.fmavlyutov.model.Task;
import org.fmavlyutov.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "show list of tasks";
    }

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public void execute() {
        System.out.println("[TASKS LIST]");
        System.out.println("Enter sort:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        final List<Task> tasks = getTaskService().findAll(Sort.toSort(sort));
        int index = 1;
        for (Task task : tasks) {
            if (task == null) {
                continue;
            }
            System.out.println(index++ + ". " + task.getName());
        }
    }

}

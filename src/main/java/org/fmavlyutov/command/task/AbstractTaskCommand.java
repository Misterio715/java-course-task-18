package org.fmavlyutov.command.task;

import org.fmavlyutov.api.service.IProjectTaskService;
import org.fmavlyutov.api.service.ITaskService;
import org.fmavlyutov.command.AbstractCommand;
import org.fmavlyutov.model.Task;

import java.util.Collection;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    protected void showTask(final Task task) {
        if (task == null) {
            return;
        }
        System.out.println(task);
    }

    protected void showTask(final Collection<Task> tasks) {
        for (final Task task : tasks) {
            showTask(task);
        }
    }

}

package org.fmavlyutov.command.task;

import org.fmavlyutov.model.Task;
import org.fmavlyutov.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "create new task";
    }

    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        final Task task = getTaskService().create(name, description);
        showTask(task);
    }

}

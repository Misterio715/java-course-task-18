package org.fmavlyutov.exception.system;

public final class ParseNumberException extends AbstractSystemException {

    public ParseNumberException(Throwable e) {
        super("Can not parse number!", e);
    }

}

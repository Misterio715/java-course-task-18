package org.fmavlyutov.exception.user;

public final class EmptyRoleException extends AbstractUserException {

    public EmptyRoleException() {
        super("Role can not be empty!");
    }

}

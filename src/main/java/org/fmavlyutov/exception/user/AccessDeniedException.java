package org.fmavlyutov.exception.user;

public final class AccessDeniedException extends AbstractUserException {

    public AccessDeniedException() {
        super("Access denied!");
    }

}

package org.fmavlyutov.exception.field;

public final class InvalidOrEmptyIdException extends AbstractFieldException {

    public InvalidOrEmptyIdException() {
        super("Invalid or empty id!");
    }

    public InvalidOrEmptyIdException(String message) {
        super(message);
    }

}

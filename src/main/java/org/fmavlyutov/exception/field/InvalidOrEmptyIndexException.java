package org.fmavlyutov.exception.field;

public final class InvalidOrEmptyIndexException extends AbstractFieldException {

    public InvalidOrEmptyIndexException() {
        super("Invalid or empty index!");
    }

}

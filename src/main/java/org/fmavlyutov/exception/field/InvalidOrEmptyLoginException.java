package org.fmavlyutov.exception.field;

public final class InvalidOrEmptyLoginException extends AbstractFieldException {

    public InvalidOrEmptyLoginException() {
        super("Invalid or empty login!");
    }

}
